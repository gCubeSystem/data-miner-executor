package org.gcube.portlets.user.dataminerexecutor.client.type;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public enum OutputResourceRequestEventType {
	ComputationId, ItemDescription;
}
