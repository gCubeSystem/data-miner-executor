<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<!--                                           -->
<!-- The module reference below is the link    -->
<!-- between html and your Web Toolkit module  -->
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/dataminerexecutor/reset.css"
	type="text/css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/DataMinerExecutor.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/gxt/css/gxt-all.css"
	type="text/css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/dataminerexecutor/css/ol.css"
	type="text/css">

<script
	src='<%=request.getContextPath()%>/dataminerexecutor/js/jquery-1.11.0.min.js'></script>
<script src='<%=request.getContextPath()%>/dataminerexecutor/js/ol.js'></script>
<script
	src='<%=request.getContextPath()%>/dataminerexecutor/js/bootstrap.min.js'></script>
<script
	src='<%=request.getContextPath()%>/dataminerexecutor/dataminerexecutor.nocache.js'></script>


<div class="contentDiv" id="contentDiv"></div>
