This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "data-miner-executor"


## [v1.4.0]

- Updated to maven-portal-bom-4.0.0
- Added support to default value for ListParameter [#24026]


## [v1.3.0] - 2021-10-06

- Added cluster description in Service Info [#19213]


## [v1.2.0] - 2019-10-02

- Added service info [#12594]
- Added support to show log information [#11711]
- Added support to show files html, json, pdf, txt [#17106]
- Updated information show to the user when a computation is submitted [#17030]
- Added Item Id support [#16503]


## [v1.1.0] - 2019-03-13

- Added automatic run [#16155]


## [v1.0.0] - 2019-01-31

- First release

